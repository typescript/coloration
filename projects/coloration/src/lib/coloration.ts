// tslint:disable:no-bitwise

const pattern = {
    hexa: /^#?(([\da-f]{3})(([\da-f]{3})([\da-f]{2})?|[\da-f]{1})?)$/i,
    rgba: /^rgba?\(([\d]*(\.[\d]+)?)(,?\s*|\s+)([\d]*(\.[\d]+)?)(,?\s*|\s+)([\d]*(\.[\d]+)?)((,\s*)([\d]*(\.[\d]+)?))?\)$/i,
    hsva: /^hsla?\(([\d]*(\.[\d]+)?)(,?\s*|\s+)([\d]*(\.[\d]+)?)\%(,?\s*|\s+)([\d]*(\.[\d]+)?)\%((,\s*)([\d]*(\.[\d]+)?))?\)$/i,
};

export interface RGB {
    /** Red color value [0,255]   */ r: number;
    /** Green color value [0,255] */ g: number;
    /** Blue color value [0,255]  */ b: number;
    /** Alpha color value [0,1]   */ a?: number;
}
export interface HSV {
    /** Hue color value [0,360]             */ h: number;
    /** Saluration color value [0,100]      */ s: number;
    /** Value/lightness color value [0,100] */ v: number;
    /** Alpha color value [0,1]             */ a?: number;
}
export interface ColorData {
    // RGB
    /** Red color value [-256,255]       */ r?: number;
    /** Green color value [-256,255]     */ g?: number;
    /** Blue color value [-256,255]      */ b?: number;
    // HSV/HSL
    /** Hue value [-360,360]             */ h?: number;
    /** Saluration value [-100,100]      */ s?: number;
    /** Value/lightness value [-100,100] */ v?: number;
    // Other
    /** luminosity value [-1,1]          */ luminosity?: number;
    /** color mask                       */ maskColor?: string;
    /** color mask opacity [0,1]         */ maskOpacity?: number;
    // alpha
    /** color mask alpha [-1,1]          */ alpha?: number;
}

export interface ColorNumber {
    /** intcolor */ intColor: number;
    /** Alpha color value [0,1] */ alpha: number;
}

export class Coloration {
    /**
     * CSS color list
     */
    static colorsName: any = {
        // CSS 1
        black: '#000000',
        silver: '#c0c0c0',
        gray: '#808080',
        white: '#ffffff',
        maroon: '#800000',
        red: '#ff0000',
        purple: '#800080',
        fuchsia: '#ff00ff',
        green: '#008000',
        lime: '#00ff00',
        olive: '#808000',
        yellow: '#ffff00',
        navy: '#000080',
        blue: '#0000ff',
        teal: '#008080',
        aqua: '#00ffff',
        // CSS 2 (Revision 1)
        orange: '#ffa500',
        // CSS 3
        aliceblue: '#f0f8ff',
        antiquewhite: '#faebd7',
        aquamarine: '#7fffd4',
        azure: '#f0ffff',
        beige: '#f5f5dc',
        bisque: '#ffe4c4',
        blanchedalmond: '#ffebcd',
        blueviolet: '#8a2be2',
        brown: '#a52a2a',
        burlywood: '#deb887',
        cadetblue: '#5f9ea0',
        chartreuse: '#7fff00',
        chocolate: '#d2691e',
        coral: '#ff7f50',
        cornflowerblue: '#6495ed',
        cornsilk: '#fff8dc',
        crimson: '#dc143c',
        cyan: '#00ffff',
        deaqua: '#00ffff',
        darkblue: '#00008b',
        darkcyan: '#008b8b',
        darkgoldenrod: '#b8860b',
        darkgray: '#a9a9a9',
        darkgreen: '#006400',
        darkgrey: '#a9a9a9',
        darkkhaki: '#bdb76b',
        darkmagenta: '#8b008b',
        darkolivegreen: '#556b2f',
        darkorange: '#ff8c00',
        darkorchid: '#9932cc',
        darkred: '#8b0000',
        darksalmon: '#e9967a',
        darkseagreen: '#8fbc8f',
        darkslateblue: '#483d8b',
        darkslategray: '#2f4f4f',
        darkslategrey: '#2f4f4f',
        darkturquoise: '#00ced1',
        darkviolet: '#9400d3',
        deeppink: '#ff1493',
        deepskyblue: '#00bfff',
        dimgray: '#696969',
        dimgrey: '#696969',
        dodgerblue: '#1e90ff',
        firebrick: '#b22222',
        floralwhite: '#fffaf0',
        forestgreen: '#228b22',
        gainsboro: '#dcdcdc',
        ghostwhite: '#f8f8ff',
        gold: '#ffd700',
        goldenrod: '#daa520',
        greenyellow: '#adff2f',
        grey: '#808080',
        honeydew: '#f0fff0',
        hotpink: '#ff69b4',
        indianred: '#cd5c5c',
        indigo: '#4b0082',
        ivory: '#fffff0',
        khaki: '#f0e68c',
        lavender: '#e6e6fa',
        lavenderblush: '#fff0f5',
        lawngreen: '#7cfc00',
        lemonchiffon: '#fffacd',
        lightblue: '#add8e6',
        lightcoral: '#f08080',
        lightcyan: '#e0ffff',
        lightgoldenrodyellow: '#fafad2',
        lightgray: '#d3d3d3',
        lightgreen: '#90ee90',
        lightgrey: '#d3d3d3',
        lightpink: '#ffb6c1',
        lightsalmon: '#ffa07a',
        lightseagreen: '#20b2aa',
        lightskyblue: '#87cefa',
        lightslategray: '#778899',
        lightslategrey: '#778899',
        lightsteelblue: '#b0c4de',
        lightyellow: '#ffffe0',
        limegreen: '#32cd32',
        linen: '#faf0e6',
        magenta: '#ff00ff',
        defuchsia: '#ff00ff',
        mediumaquamarine: '#66cdaa',
        mediumblue: '#0000cd',
        mediumorchid: '#ba55d3',
        mediumpurple: '#9370db',
        mediumseagreen: '#3cb371',
        mediumslateblue: '#7b68ee',
        mediumspringgreen: '#00fa9a',
        mediumturquoise: '#48d1cc',
        mediumvioletred: '#c71585',
        midnightblue: '#191970',
        mintcream: '#f5fffa',
        mistyrose: '#ffe4e1',
        moccasin: '#ffe4b5',
        navajowhite: '#ffdead',
        oldlace: '#fdf5e6',
        olivedrab: '#6b8e23',
        orangered: '#ff4500',
        orchid: '#da70d6',
        palegoldenrod: '#eee8aa',
        palegreen: '#98fb98',
        paleturquoise: '#afeeee',
        palevioletred: '#db7093',
        papayawhip: '#ffefd5',
        peachpuff: '#ffdab9',
        peru: '#cd853f',
        pink: '#ffc0cb',
        plum: '#dda0dd',
        powderblue: '#b0e0e6',
        rosybrown: '#bc8f8f',
        royalblue: '#4169e1',
        saddlebrown: '#8b4513',
        salmon: '#fa8072',
        sandybrown: '#f4a460',
        seagreen: '#2e8b57',
        seashell: '#fff5ee',
        sienna: '#a0522d',
        skyblue: '#87ceeb',
        slateblue: '#6a5acd',
        slategray: '#708090',
        slategrey: '#708090',
        snow: '#fffafa',
        springgreen: '#00ff7f',
        steelblue: '#4682b4',
        tan: '#d2b48c',
        thistle: '#d8bfd8',
        tomato: '#ff6347',
        turquoise: '#40e0d0',
        violet: '#ee82ee',
        wheat: '#f5deb3',
        whitesmoke: '#f5f5f5',
        yellowgreen: '#9acd32',
        // CSS 4
        rebeccapurple: '#663399',
    };

    private calcColor: ColorNumber;
    private rgb: RGB = { r: 0, g: 0, b: 0, a: 1 };
    private hsv: HSV = { h: 0, s: 0, v: 0, a: 1 };

    constructor(public color: string) {
        this.reset();
    }

    /**
     * reinit to base color
     */
    reset() {
        if (this.color) {
            this.calcColor = this.parseColor(this.color);
            this.updateColor();
        }
    }

    /**
     * change the luminosity of a color
     * @param lum value between -1 and 1
     * @returns Coloration
     */
    changeLuminosity(lum: number): Coloration {
        lum = this.minmax(lum, -1, 1);
        this.maskColor(lum < 0 ? '#000' : '#FFF', Math.abs(lum));
        return this;
    }

    /**
     * add color with a mark
     * @param color additional color
     * @param opacity value of opacity between 0 and 1 for the additional color
     * @returns Coloration
     */
    maskColor(color: string, opacity: number = 1): Coloration {
        if (this.calcColor) {
            const baseColor = this.calcColor;
            const additionalColor = this.parseColor(color);

            const lum = this.minmax(opacity, 0, 1);

            const R = baseColor.intColor >> 16;
            const G = (baseColor.intColor >> 8) & 0x00ff;
            const B = baseColor.intColor & 0x0000ff;

            if (additionalColor.alpha) {
                this.calcColor.alpha = this.minmax(baseColor.alpha + additionalColor.alpha * opacity, 0, 1);
            }
            this.calcColor.intColor = this.rgbToInt(
                Math.round(((additionalColor.intColor >> 16) - R) * lum) + R,
                Math.round((((additionalColor.intColor >> 8) & 0x00ff) - G) * lum) + G,
                Math.round(((additionalColor.intColor & 0x0000ff) - B) * lum) + B,
            );
            this.updateColor();
        }
        return this;
    }

    /**
     * change color with color parameters
     * @param colorData additionnal parameters
     * @returns Coloration
     */
    addColor(colorData: ColorData) {
        if (this.calcColor) {
            if (colorData.luminosity) {
                this.changeLuminosity(colorData.luminosity);
            }

            if (colorData.maskColor) {
                this.maskColor(colorData.maskColor, this.minmax(colorData.maskOpacity || 0, 0, 1));
            }

            if (colorData.r || colorData.g || colorData.b) {
                if (colorData.r) {
                    this.rgb.r = this.minmax(this.rgb.r + +colorData.r, 0, 255);
                }
                if (colorData.g) {
                    this.rgb.g = this.minmax(this.rgb.g + +colorData.g, 0, 255);
                }
                if (colorData.b) {
                    this.rgb.b = this.minmax(this.rgb.b + +colorData.b, 0, 255);
                }
                this.calcColor.intColor = this.rgbToInt(this.rgb.r, this.rgb.g, this.rgb.b);
                this.updateColor();
            }

            if (colorData.h || colorData.s || colorData.v) {
                if (colorData.h) {
                    this.hsv.h = (this.hsv.h + +colorData.h + 360) % 360;
                }
                if (colorData.s) {
                    this.hsv.s = this.minmax(this.hsv.s + +colorData.s, 0, 100);
                }
                if (colorData.v) {
                    this.hsv.v = this.minmax(this.hsv.v + +colorData.v, 0, 100);
                }
                this.calcColor.intColor = this.hsvToInt(this.hsv.h, this.hsv.s, this.hsv.v);
                this.updateColor();
            }

            if (colorData.alpha) {
                this.hsv.a =
                    this.rgb.a =
                    this.calcColor.alpha =
                        this.minmax(this.calcColor.alpha + +colorData.alpha, 0, 1);
            }
        }
        return this;
    }

    /**
     * RGB informations
     * @returns RGB
     */
    getRGB(): RGB {
        return this.calcColor ? this.value<RGB>(() => Object.assign({}, this.rgb)) : null;
    }

    /**
     * HSV informations
     * @returns HSV
     */
    getHSV(): HSV {
        return this.calcColor ? this.value<HSV>(() => Object.assign({}, this.hsv)) : null;
    }

    /**
     * color in #HEX format
     * @returns string of #HEX
     */
    toHEX(): string {
        return this.calcColor
            ? this.value<string>(
                  () =>
                      '#' +
                      (0x1000000 + this.calcColor.intColor).toString(16).slice(1) +
                      (this.calcColor.alpha < 1
                          ? (0x100 + Math.round(this.calcColor.alpha * 255)).toString(16).slice(1)
                          : ''),
              )
            : null;
    }

    /**
     * color in HVL() format
     * @returns string of HVL(H, S%, V%)
     */
    toHSL(): string {
        return this.calcColor
            ? this.value<string>(
                  () => `hsl(${this.hsv.h}, ${this.hsv.s}%, ${this.hsv.v}%${this.hsv.a < 1 ? ', ' + this.hsv.a : ''})`,
              )
            : null;
    }

    /**
     * color in RGB() format
     * @returns string of RGB(R, G, B)
     */
    toRGB(): string {
        return this.calcColor
            ? this.value<string>(
                  () => `rgb(${this.rgb.r}, ${this.rgb.g}, ${this.rgb.b}${this.rgb.a < 1 ? ', ' + this.rgb.a : ''})`,
              )
            : null;
    }

    /**
     * update colors data : RGB and HSL
     */
    private updateColor() {
        const color = this.calcColor.intColor;
        // update RGB
        this.rgb.r = color >> 16;
        this.rgb.g = (color >> 8) & 0x00ff;
        this.rgb.b = color & 0x0000ff;
        this.rgb.a = this.calcColor.alpha;

        // update HSL
        this.hsv = this.rgb2hsv(this.rgb.r, this.rgb.g, this.rgb.b);
        this.hsv.a = this.calcColor.alpha;
    }

    /**
     * change color
     * @param color parse color : name, #HEXA, rgba()
     * @returns int color
     */
    private parseColor(color: string): ColorNumber {
        // si named color
        if (Coloration.colorsName[color]) {
            color = Coloration.colorsName[color];
        }

        // validate hexa string #RGB, #RGBA, #RRGGBB, #RRGGBBAA (ignore alpha)
        const matchHex = String(color).match(pattern.hexa);
        let intColor: number;
        let alpha = 1;
        if (matchHex) {
            const hexaColor = matchHex[4]
                ? matchHex[2] + matchHex[4]
                : matchHex[2][0] + matchHex[2][0] + matchHex[2][1] + matchHex[2][1] + matchHex[2][2] + matchHex[2][2];
            const hexaAlpha = matchHex[4] ? matchHex[5] : matchHex[3] ? matchHex[3][1] + matchHex[3][1] : undefined;
            intColor = parseInt(hexaColor, 16);
            alpha = hexaAlpha !== undefined ? parseInt(hexaAlpha, 16) / 255 : 1;
        }

        if (intColor === undefined) {
            // validate rgb() / rgba()
            const matchRgb = String(color).match(pattern.rgba);
            if (matchRgb) {
                intColor = this.rgbToInt(
                    parseInt(matchRgb[1], 10),
                    parseInt(matchRgb[4], 10),
                    parseInt(matchRgb[7], 10),
                );
                alpha = matchRgb[11] !== undefined ? parseFloat(matchRgb[11]) : 1;
            }
        }

        if (intColor === undefined) {
            // validate hsv() / hsva()
            const matchHsv = String(color).match(pattern.hsva);
            if (matchHsv) {
                intColor = this.hsvToInt(
                    parseInt(matchHsv[1], 10),
                    parseInt(matchHsv[4], 10),
                    parseInt(matchHsv[7], 10),
                );
                alpha = matchHsv[11] !== undefined ? parseFloat(matchHsv[11]) : 1;
            }
        }

        return { intColor, alpha };
    }

    /**
     * convert HSV/HSL to int RGB
     * @param hue Hue [0, 360]
     * @param saturation Saturation [0, 100]
     * @param value Value [0, 100]
     * @returns int RGB
     * @see https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
     */
    private hsvToInt(hue: number, saturation: number, value: number): number {
        saturation = Math.max(0, saturation);

        const s = saturation / 100;
        const v = value / 100;
        const h = hue / 360;

        if (saturation === 0) {
            return this.rgbToInt(v * 256, v * 256, v * 256);
        }

        const q = v < 0.5 ? v * (1 + s) : v + s - v * s;
        const p = 2 * v - q;
        const r = this.hue2rgb(p, q, h + 1 / 3);
        const g = this.hue2rgb(p, q, h);
        const b = this.hue2rgb(p, q, h - 1 / 3);

        return this.rgbToInt(Math.round(r * 255), Math.round(g * 255), Math.round(b * 255));
    }

    /**
     * hue color calculation
     * @param p number
     * @param q number
     * @param t number
     * @returns int color [0, 255]
     */
    private hue2rgb(p: number, q: number, t: number): number {
        if (t < 0) {
            t += 1;
        } else if (t > 1) {
            t -= 1;
        }
        if (t < 1 / 6) {
            return p + (q - p) * 6 * t;
        }
        if (t < 1 / 2) {
            return q;
        }
        if (t < 2 / 3) {
            return p + (q - p) * (2 / 3 - t) * 6;
        }
        return p;
    }

    /**
     * Convert RGB to HSV
     * @param r red [0, 255]
     * @param g green [0, 255]
     * @param b blue [0, 255]
     * @returns HSV data
     * @see https://stackoverflow.com/questions/39118528/rgb-to-hsl-conversion
     * @see https://en.wikipedia.org/wiki/HSL_and_HSV#Formal_derivation
     */
    private rgb2hsv(r: number, g: number, b: number): HSV {
        // convert r,g,b [0,255] range to [0,1]
        r = r / 255;
        g = g / 255;
        b = b / 255;

        // get the min and max of r,g,b
        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);

        // lightness is the average of the largest and smallest color components
        const val = (max + min) / 2;
        let hue: number;
        let sat: number;

        if (max === min) {
            // no saturation
            hue = 0;
            sat = 0;
        } else {
            const c = max - min; // chroma
            // saturation is simply the chroma scaled to fill
            // the interval [0, 1] for every combination of hue and lightness
            sat = c / (1 - Math.abs(2 * val - 1));
            switch (max) {
                case r:
                    hue = (g - b) / c + (g < b ? 6 : 0);
                    break;
                case g:
                    hue = (b - r) / c + 2;
                    break;
                case b:
                    hue = (r - g) / c + 4;
                    break;
            }
        }

        return {
            h: Math.round(hue * 60), // °
            s: Math.round(sat * 100), // %
            v: Math.round(val * 100), // %
        };
    }

    /**
     * convert RGB data to int value
     * @param r red [0, 255]
     * @param g green [0, 255]
     * @param b blue [0, 255]
     * @returns int value
     */
    private rgbToInt(r: number, g: number, b: number): number {
        return Math.round(r) * 0x10000 + Math.round(g) * 0x100 + Math.round(b);
    }

    /**
     * bound a value between two values
     * @param value value
     * @param min min value
     * @param max max value
     * @param defaultValue replace an invalid vvalue
     * @returns value between min and max
     */
    private minmax(value: number, min: number, max: number, defaultValue = 0) {
        return Math.min(Math.max(value || defaultValue, min), max);
    }

    /**
     * test is intColor is valid for return a vlaue
     * @param value callback of value
     * @returns value or null
     */
    private value<L>(value: () => L): L {
        return !isNaN(this.calcColor.intColor) ? value() : null;
    }
}

# Changelog of coloration-lib

## V0.2.0 (2022-05-30)

### Corrections

-   with Ivy partial compilation mode
-   use prettier

## V0.1.3 (2020-06-02)

### Corrections

-   tslint `^2.0.0`

## V0.1.2 (2020-05-04)

### Corrections

-   fix alpha calculation for `toHEX()`

## V0.1.1 (2020-05-02)

### Feature

-   new design for the demo

### Corrections

-   fix calculation when no alpha
-   fix alpha calculation with mask opacity

## V0.1.0 (2020-05-01)

### Feature

-   add alpha support

### Corrections

-   add more control
-   fix blue additionnal
-   fix bug with float values for rgb
-   update demo

## V0.0.4 (2019-01-23)

### Feature

-   add `reset()` method

## V0.0.3 (2019-01-22)

### Features

-   add `maskColor` and `maskOpacity` params for `addColor()`

## V0.0.2 (2019-01-21)

### Features

-   add `getRGB()` and `getHSV()` methods

## V0.0.1 (2019-01-20)

-   initial release

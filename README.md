[![npm version](https://badge.fury.io/js/coloration-lib.svg)](https://badge.fury.io/js/coloration-lib) [![Downloads](https://img.shields.io/npm/dm/coloration-lib.svg)](https://www.npmjs.com/package/coloration-lib) [![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](https://git.ikilote.net/angular/coloration/raw/master/LICENSE)

# Coloration

## Installation

```
npm i coloration-lib --save
```

-   0.1.3 : for View Engine
-   0.2.0+ : for Ivy

## Requirements

Only for demo:

-   Angular 13.3.0 and more

## Demo

[See a live demonstation](http://test.ikilote.net/coloration-demo/)

## Usage

### Examples

#### changeLuminosity

```typescript
import { Coloration } from 'coloration-lib';

const color = new Coloration('red');
color.changeLuminosity(0.55);
console.log(color.toHEX()); // #ff8c8c
```

#### maskColor

```typescript
const color = new Coloration('red');
color.maskColor('blue', 0.25);
console.log(color.toHEX()); // #bf0040
```

#### addColor

```typescript
const color = new Coloration('red');
color.addColor({
    h: -23,
    v: 10,
    alpha: -0.2,
});
console.log(color.toHEX()); // #ff3381cc
```

## Publishing the library

```
npm run build:lib
npm run publish:lib
```

## Publishing the demo

```
npm run build:demo
```

## License

This module is released under the permissive MIT license. Your contributions are always welcome.

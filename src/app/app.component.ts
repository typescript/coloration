import { Component } from '@angular/core';

import { Coloration } from 'projects/coloration/src/public_api';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    baseColor = '';

    luminosity = 0.0;
    red = 0;
    green = 0;
    blue = 0;
    hue = 0;
    saturation = 0;
    value = 0;
    maskColor = '';
    maskOpacity = 1;
    alpha = 0;

    colorHex: string;
    colorRgb: string;
    colorHsl: string;

    private color: Coloration;

    constructor() {}

    change() {
        if (this.baseColor) {
            this.color = new Coloration(this.baseColor);
            this.color.addColor({
                r: this.red,
                g: this.green,
                b: this.blue,
                h: this.hue,
                s: this.saturation,
                v: this.value,
                luminosity: this.luminosity,
                maskColor: this.maskColor,
                maskOpacity: this.maskOpacity,
                alpha: this.alpha,
            });

            this.colorHex = this.color.toHEX();
            this.colorHsl = this.color.toHSL();
            this.colorRgb = this.color.toRGB();
        }
    }

    get gradient() {
        const hexa = this.color ? this.color.toHEX() : undefined;
        return this.color && hexa ? 'linear-gradient(to right, ' + this.baseColor + ', ' + hexa + ')' : this.baseColor;
    }
}
